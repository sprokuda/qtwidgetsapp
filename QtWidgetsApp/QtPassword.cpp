#include "QtPassword.h"

QtPassword::QtPassword(const QFont& qfont, QWidget* parent)
		:QWidget(parent), font(qfont)
{
	today = QDate::currentDate();
	int dow = 0;
	if (today.dayOfWeek() < 7) dow = today.dayOfWeek() + 1;
	else dow = 1;
	password = QString::number(today.day() * 13 * (dow + 21) + today.month() * 27);


	label = new QLabel("Enter password:", this);
	edit = new QLineEdit(this);
	okButton = new QPushButton("Conform",this);
	cancelButton = new QPushButton("Exit", this);

	QSpacerItem* spacer = new QSpacerItem(120,1);

	box = new QMessageBox(this);
	box->setText("Wrong password!");
	box->addButton(QMessageBox::Ok);
	box->setIcon(QMessageBox::Warning);
	box->setFont(font);


	QHBoxLayout* buttonLayout = new QHBoxLayout();
	buttonLayout->addStretch();
	buttonLayout->addWidget(okButton);
	buttonLayout->addStretch();
	buttonLayout->addWidget(cancelButton);
	buttonLayout->addStretch();

	QVBoxLayout* mainLayout= new QVBoxLayout(this);
	mainLayout->addWidget(label);
	mainLayout->addWidget(edit);
	mainLayout->addLayout(buttonLayout);

	setFont(qfont);

	setLayout(mainLayout);

	connect(edit, SIGNAL(textChanged(QString)), this, SLOT(onTextChanged(QString)));
	connect(edit, SIGNAL(editingFinished()), this, SLOT(onPasswordEntered()));
	connect(okButton, SIGNAL(clicked()), this, SLOT(onPasswordEntered()));
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(onCancelClicked()));
}

void QtPassword::show()
{
	QWidget::show();
}

void QtPassword::onTextChanged(QString new_text)
{
	QString text_to_show;

//	text = text.append(QString(new_text).remove("*"));
	int new_position = 0;
	for (int i = 0; i < new_text.size(); i++)
	{
		if (new_text.at(i) != QChar('*'))
		{
			new_position = i;
			break;
		}
	}
	
	text = text.insert(new_position, QString(new_text).remove("*"));

	for (int i = 0; i < new_text.size(); i++)
	{
		text_to_show.append("*");
	}
	edit->setText(text_to_show);
	label->setText(text);
}

void QtPassword::onPasswordEntered()
{

	if (text == password)
	{
		hide();
		emit passwordAccepted();
	}
	else
	{
		box->exec();
	}
}

void QtPassword::onCancelClicked()
{
	hide();
	emit passwordRejected();
}

