#pragma once

#include <QtWidgets/QWidget>
#include <QLabel.h>
#include <QLayout>

#include "../QtClassLib/include/datepicker.h"
#include "../QtClassLib/include/datepickerhumanreadableformater.h"

#include "QtDatePicker.h"
#include "QtMultiSelect.h"
#include "QtWaitingSpinner.h"

class QtWidgetsApp : public QWidget
{
    Q_OBJECT

public:
    QtWidgetsApp(QWidget *parent = Q_NULLPTR);

private slots:
    void onShowWS();

private:
    QtDatePicker* datePicker;
    QtMultiSelect* multiSelect;
    DatePicker* picker;

    QPushButton* showWS;
    QtWaitingSpinner* sp;

    void moveEvent(QMoveEvent* event);
    void resizeEvent(QResizeEvent* event);

};
