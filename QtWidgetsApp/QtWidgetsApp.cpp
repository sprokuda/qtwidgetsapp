#include "QtWidgetsApp.h"


QtWidgetsApp::QtWidgetsApp(QWidget *parent)
    : QWidget(parent)
{
    QStringList tmp = { "0001","0002","0005" };

    QFont font("Calibri", 11);
    int buttonHeight = 24;

    this->setFont(font);

    datePicker = new QtDatePicker(this);
    QHBoxLayout* pickerLayout = new QHBoxLayout();
    //picker = new DatePicker;
    //picker->setDatePeriod(QDate(1900, 1, 1), QDate(2100, 12, 31));
    pickerLayout->addStretch();
    pickerLayout->addWidget(datePicker);

    multiSelect = new QtMultiSelect(this);
    multiSelect->getPopup().setTable(tmp);

    showWS = new QPushButton(this);

    sp = new QtWaitingSpinner(this);

    //QListWidget* qlw = new QListWidget(this);
    //QListWidgetItem* qlwi =new QListWidgetItem();
    //qlw->addItem(qlwi);
    //qlw->setItemWidget(qlwi, datePicker);

    QVBoxLayout* mainLayout = new QVBoxLayout();
//    mainLayout->addWidget(datePicker);
    mainLayout->addLayout(pickerLayout);
//    mainLayout->addWidget(qlw);

    mainLayout->addStretch();
    mainLayout->addWidget(multiSelect);
    mainLayout->addStretch();
    mainLayout->addWidget(showWS);

    setLayout(mainLayout);
//    this->resize(300, 300);

//    move(800, 400);

    connect(showWS, SIGNAL(clicked()), SLOT(onShowWS()));

}

void QtWidgetsApp::onShowWS()
{
    sp->show();
    sp->adjustPosition();
}

void QtWidgetsApp::moveEvent(QMoveEvent* event)
{
    QWidget::moveEvent(event);
    this->datePicker->adjustPopupPosition();
    this->multiSelect->adjustPopupPosition();
    this->sp->adjustPosition();

}

void QtWidgetsApp::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
    this->datePicker->adjustPopupPosition();
    this->multiSelect->adjustPopupPosition();
    this->sp->adjustPosition();

}
