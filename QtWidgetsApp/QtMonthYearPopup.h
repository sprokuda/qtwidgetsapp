#pragma once

#include <QtWidgets/QWidget>
#include <QLayout.h>
#include <QCalendarWidget>
#include <QPushButton>
#include <QHeaderView>

#include <QDesktopWidget>
#include <QApplication>
#include <QPaintEvent>
#include <QPainter>
#include <QPainterPath>
#include <QGraphicsDropShadowEffect>

class QtMonthYearPopup : public QWidget
{
    Q_OBJECT

public:
    QtMonthYearPopup(QWidget* parent = Q_NULLPTR);

signals:
    void dateSelected(const QDate& date);
    //    void datePeriodSelected(const QDate& begin, const QDate& end);

private:

    QCalendarWidget* calendar;
    QPushButton* hideButton;
    void paintEvent(QPaintEvent* event);
    //    void resizeEvent(QResizeEvent* event);
    //    void moveEvent(QMoveEvent* event);

private slots:
    void onCalendarDateSelected(const QDate& date);
};
