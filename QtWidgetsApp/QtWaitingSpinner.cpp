#include "QtWaitingSpinner.h"
#include <QMovie>
#include <QLayout>



QtWaitingSpinner::QtWaitingSpinner(QWidget* parent)
    : QWidget(parent)
{
    label = new QLabel(this);

    QMovie* movie = new QMovie(":/new/prefix1/resources/ajax-loader.gif");
    //QMovie* movie = new QMovie("ajax-loader.gif");
    label->setMovie(movie);
//    label->show();
    movie->start();

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(label);
    mainLayout->setSizeConstraint(QLayout::SetFixedSize);
    setLayout(mainLayout);

    this->setAttribute(Qt::WA_TranslucentBackground);

    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::Window);


}



void QtWaitingSpinner::adjustPosition()
{
    const QPoint global = parentWidget()->mapToGlobal(parentWidget()->rect().center());
    this->move(global.x() - width()/2 , global.y() - height()/2);//edit->height()//- edit->width()/2
}

