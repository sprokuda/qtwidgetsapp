#include "QtWidgetsApp.h"
#include <QtWidgets/QApplication>
#include "QtPassword.h"

#include <QIcon>

#include <iostream>
#include <filesystem>

using namespace std;
using namespace filesystem;

QString workingDirectory;
QFont workingFont;

int main(int argc, char *argv[])
{
    QLocale::setDefault(QLocale(QLocale::English, QLocale::Australia));
    QApplication app(argc, argv);
    workingFont = QFont("Calibri", 10);
    workingDirectory = QString::fromWCharArray(weakly_canonical(path(argv[0])).parent_path().c_str());
    app.setWindowIcon(QIcon(workingDirectory + "\\centaur-icon.png"));
    
    QtWidgetsApp qw;
//    qw.show();

    //QFont font("Calibri", 11);
    //int buttonHeight = 24;
    QtPassword qp(workingFont);

    QObject::connect(&qp, SIGNAL(passwordAccepted()), &qw, SLOT(show()));
    QObject::connect(&qp, SIGNAL(passwordRejected()), &app, SLOT(quit()));
    qp.show();

    return app.exec();
}
