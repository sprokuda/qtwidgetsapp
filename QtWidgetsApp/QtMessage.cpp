#include "QtMessage.h"


QtMessage::QtMessage(const QFont& qfont, const QString& text, QWidget* parent)
	:QWidget(parent), font(qfont), text(text)
{
	label = new QLabel(text, this);

	QHBoxLayout* labelLayout = new QHBoxLayout();
	labelLayout->addStretch();
	labelLayout->addWidget(label);
	labelLayout->addStretch();

	okButton = new QPushButton("Ok", this);
//	cancelButton = new QPushButton("Cancel", this);

	QHBoxLayout* buttonLayout = new QHBoxLayout();
	buttonLayout->addStretch();
	buttonLayout->addWidget(okButton);
	buttonLayout->addStretch();
//	buttonLayout->addWidget(cancelButton);
//	buttonLayout->addStretch();

	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	mainLayout->addLayout(labelLayout);
	mainLayout->addLayout(buttonLayout);

	setFont(qfont);

	setLayout(mainLayout);


	connect(okButton, SIGNAL(clicked()), this, SLOT(hide()));
//	connect(cancelButton, SIGNAL(clicked()), this, SLOT(onCancelClicked()));
}

void QtMessage::moveEvent(QMoveEvent* event)
{
    QWidget::moveEvent(event);

//	this->move(event->pos());

    //QRect geometry = this->geometry();
    //geometry.moveTo(parentWidget()->mapToGlobal(parentWidget()->rect().bottomLeft()));
    //QRect screen_geometry = QApplication::desktop()->screenGeometry();
    //move(geometry.topLeft());
}




