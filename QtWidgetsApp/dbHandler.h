#pragma once

#include <QObject>
#include <iostream>

class dbHandler : public QObject
{
    Q_OBJECT
public:
    explicit dbHandler(QObject* parent = nullptr);
    ~dbHandler();

    Q_INVOKABLE void doDelay();
    Q_INVOKABLE void queryAppBook();

signals:
    void allCompleted();
    void appBookReady(QStringList list);

};

