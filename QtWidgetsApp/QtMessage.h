#pragma once

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QMessageBox>
#include <QDate>
#include <QLayout>
#include <QKeyEvent>

class QtMessage : public QWidget
{
    Q_OBJECT

public:
    QtMessage(const QFont& qfont, const QString& text, QWidget* parent = Q_NULLPTR);

private:
    QFont font;
    int buttonHeight;
    const QString text;
    QLabel* label;
    QPushButton* okButton;
    QPushButton* cancelButton;

    void moveEvent(QMoveEvent* event);

signals:


private slots:


};

